# \# Issues closed

Number of closed issues in a period of time.

```
index: 'all'
range: from_date < 'closed_at' < to_date
filter: pull_request:False or is_gitlab_issue:1
filter: 'state' is 'closed'
```
