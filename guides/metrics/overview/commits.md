# \# Commits

Number of commits, ignoring pull/merge request commits.

```
index: 'git'
unique count: 'hash'
range: from_date < 'grimoire_creation_date' < to_date
filter: 'files' is not 0
```
