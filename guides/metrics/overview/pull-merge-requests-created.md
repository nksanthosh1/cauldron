# \# PRs/MRs created

Number of new Pull/Merge Requests in a period of time.

```
index: 'all'
range: from_date < 'created_at' < to_date
filter: pull_request:True or merge_request:True
```
