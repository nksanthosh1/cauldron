# Project metrics

Each project in Cauldron provides some basic metrics about the repositories or other data that you may have analyzed.

These metric are divided in 2 categories, and an overview panel that includes metrics and charts from both sections.

Also, for those interested in further software development metrics, there is the possibility of viewing the results of your analysis in [Kibana](https://opendistro.github.io/for-elasticsearch-docs/docs/kibana/), through the `More details` button, found at the end of the sections.

## Overview

This section shows an overview metrics panel about the analyzed data (mainly git). The metrics and graphs included in this section are:
- [\# Commits](metrics/overview/commits.md)
- [\# Pull requests](metrics/overview/pull-merge-requests-created.md)
- [Median review duration](metrics/overview/median-review-duration.md)
- [\# Issues created](metrics/overview/issues-created.md)
- [\# Issues closed](metrics/overview/issues-closed.md)
- [Median time to close](metrics/overview/median-time-to-close.md)
- [\# Commits over time (chart)](metrics/activity/commits-over-time.md)
- [\# Authors per category over time (chart)](metrics/overview/authors-evolution.md)
- [\# Issues open/closed (chart)](metrics/activity/issues-open-closed.md)
- [\# Reviews open/closed (chart)](metrics/activity/reviews-open-closed.md)

## Activity

This section shows metrics related to the activity of the analyzed data (mainly git). The metrics and graphs included in this section are:
- [\# Commits](metrics/activity/commits.md)
- [\# Lines/commit](metrics/activity/lines-commit.md)
- [\# Lines/commit/file](metrics/activity/lines-commit-file.md)
- [\# Commits over time (chart)](metrics/activity/commits-over-time.md)
- [\# Lines added/removed (chart)](metrics/activity/lines-added-removed.md)
- [\# Commits by hour of the day (chart)](metrics/activity/commits-by-hour-of-day.md)
- [\# Commits by weekday (chart)](metrics/activity/commits-by-weekday.md)
- [\# Commits by hour and weekday (local time of commit author)](metrics/activity/commits-heatmap.md)
- [\# Issues created](metrics/activity/issues-created.md)
- [\# Issues closed](metrics/activity/issues-closed.md)
- [\# Issues open](metrics/activity/issues-open.md)
- [\# Issues open/closed (chart)](metrics/activity/issues-open-closed.md)
- [\# Open issues age (chart)](metrics/activity/open-issues-age.md)
- [\# Issues open by weekday (chart)](metrics/activity/issues-open-by-weekday.md)
- [\# Issues closed by weekday (chart)](metrics/activity/issues-closed-by-weekday.md)
- [Issues Opened Heatmap](metrics/activity/issues-opened-heatmap.md)
- [Issues Closed Heatmap](metrics/activity/issues-closed-heatmap.md)
- [\# Pull/Merge requests created](metrics/activity/pull-merge-requests-created.md)
- [\# Pull/Merge requests closed](metrics/activity/pull-merge-requests-closed.md)
- [\# Pull/Merge requests open](metrics/activity/pull-merge-requests-open.md)
- [\# Reviews open/closed (chart)](metrics/activity/reviews-open-closed.md)
- [\# Open reviews age (chart)](metrics/activity/open-reviews-age.md)
- [\# Reviews open by weekday (chart)](metrics/activity/reviews-open-by-weekday.md)
- [\# Reviews closed by weekday (chart)](metrics/activity/reviews-closed-by-weekday.md)
- [Reviews Opened Heatmap](metrics/activity/reviews-opened-heatmap.md)
- [Reviews Closed Heatmap](metrics/activity/reviews-closed-heatmap.md)

## Community

This section shows metrics related to the community activity of the analyzed data (mainly git). The metrics and graphs included in this section are:
- [\# Active people](metrics/community/active-people.md)
- [\# Onboardings](metrics/community/onboardings.md)
- [Active authors (Git)](metrics/community/authors-commits.md)
- [Active submitters (Issues)](metrics/community/authors-issues.md)
- [Active submitters (PRs/MRs)](metrics/community/authors-reviews.md)
- [Authors onboarding / last active (Git)](metrics/community/onboarding-last-active-git.md)
- [Submitters onboarding / last active (Issues)](metrics/community/onboarding-last-active-issues.md)
- [Submitters onboarding / last active (PRs/MRs)](metrics/community/onboarding-last-active-reviews.md)
- [Authors aging (Git)](metrics/community/aging-git.md)
- [Submitters aging (Issues)](metrics/community/aging-issues.md)
- [Submitters aging (PRs/MRs)](metrics/community/aging-reviews.md)
- [Authors retained ratio (Git)](metrics/community/retained-ratio-git.md)
- [Submitters retained ratio (Issues)](metrics/community/retained-ratio-issues.md)
- [Submitters retained ratio (PRs/MRs)](metrics/community/retained-ratio-reviews.md)
- [Organizational diversity (Git) - Authors](metrics/community/organizational-diversity-authors.md)
- [Organizational diversity (Git) - Commits](metrics/community/organizational-diversity-commits.md)
